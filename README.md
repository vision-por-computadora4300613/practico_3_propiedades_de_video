# Practico_3_Propiedades_de_Video



## CONSIGNA

1. ¿Cómo obtener el número de cuadros por segundo o fps de un video guardado usando
las OpenCV? Usar luego para no tener que harcodear el delay del waitKey, es decir
que el programa muestre el siguiente cuadro con el retardo de tiempo que corresponde
a como fue grabado.
2. ¿Cómo obtener el ancho y alto de las imágenes capturadas usando las OpenCV? Usar
para dar dimensiones al video generado, es decir para no tener que harcodear el frameSize

## HOW TO USE IT

Para correr el script, simplemente:

    python3 propiedadesDeVideo.py prueba.mp4 

Lo que hace este programa es convertir el video que se le pase por parámetro en un video en blanco y negro.
Para salir, apretar 'q' o dejar que termine el video. 

