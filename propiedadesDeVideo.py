#! /usr/bin/env python
#-*-coding:utf-8-*-

import sys 
import cv2

#Obtenemos el video desde el argumento de la llamada al programa
if (len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pass a filename as first argument')
    sys.exit(0)

#Abrimos la captura del video
cap = cv2.VideoCapture(filename)

#Le decimos el codec, en este caso para MP4
fourcc = cv2.VideoWriter_fourcc(*'mp4v')

#Seteamos ancho y alto con las funciones que nos da cv2
width = int (cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height =int (cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

#Seteamos los fps también con la función de cv2
fps = cap.get(cv2.CAP_PROP_FPS)

print("-----------------------------------------------------------------\n")
print("Propiedades de Video\n\n")
print("FPS: "+ str(fps))
print("WIDTH: " + str(width))
print("HEIGHT: "+ str(height))
print("-----------------------------------------------------------------\n")

#Inicializamos el objeto que luego se encarga de guardar el video final
out = cv2.VideoWriter('output.mp4', fourcc, fps, (width, height))

#Este delay es el que va entre fotograma y fotograma
delay = int(1000/fps)

#Recorre todos los fotogramas que hay en la captura de video
for frame_idx in range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))):

    #Leemos cada fotograma de la captura
    ret, frame = cap.read()
    
    #Volvemos gris cada fotograma
    #IMPORTANTE: luego de esto cada fotograma tendrá solo un canal (antes tenía 3 porque era RGB)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #Para que vuelva a tener 3 canales y lo podamos guardar, es necesario agregarles los otros 2, simplemente 'mergeando' 3 iguales
    gray3 = cv2.merge([gray,gray,gray])

    #Mostramos el resultado y lo guardamos
    cv2.imshow('Image gray', gray3)
    out.write(gray3)


    if cv2.waitKey(delay) & 0xFF == ord('q'):
        break



#Cerramos todas las ventanas y procesos
out.release()
cap.release()
cv2.destroyAllWindows()
